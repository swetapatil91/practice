#!/usr/bin/env bash

DIR="$( cd "$( dirname "$0" )" >/dev/null 2>&1 && pwd )"
DRUPAL_ROOT=$(dirname ${DIR})
DRUSH="${DRUPAL_ROOT}/bin/drush"
CONSOLE="${DRUPAL_ROOT}/bin/drupal"

cd ${DRUPAL_ROOT}

# Install packages.
composer install --no-progress --no-suggest --no-interaction --optimize-autoloader --prefer-dist --ignore-platform-reqs
STATUS=$?
if [[ ${STATUS} -ne 0 ]]; then
  echo "Composer install failed."
  exit ${STATUS}
fi

# Run profile build script.
chmod +x docroot/profiles/custom/connectid/scripts/*.sh
docroot/profiles/custom/connectid/scripts/build.sh
