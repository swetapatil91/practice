# How to add the styles for new site

This tutorial shows how to create styles for a new site with atomic components.

## Preparation

Depends on required tier, need to create **scss** folder inside _project directory_ (**investis_project_t2**  or  **investis_project_t1**) 

#### Inside **scss** folder must be 4 files:
 - _palette.scss
```
# Here must be placed a list of variables with client brand colors
# For example: 

$primary:      #03a9f4;
$secondary:    #ffeb3b;
...
```
 - _variables.scss
```
# Here must be placed a list of variables that override the existing ones from the platform.
# For example: 
$font-regular: 'Helvetica Neue';
$link-color:   $primary;
...
```
 - _skin.scss
```
# This is a canvas for all additional styles specific to this particular site
# For example: 

.some-class {
  margin-bottom: 10px;
}
...
```
 - main.scss
```sass
// This is main file, where placed base includes. For correct overriding need to care about the order
// Pay attention to **style_t2** file. 
// It must be changed to **style_t1** for **investis_project_t1** folder

@import '../../../../../themes/investis_front/components/patterns/00-base/palette';
@import './palette';
@import '../../../../../themes/investis_front/components/patterns/00-base/variables';
@import './variables';
@import '../../../../../themes/investis_front/components/patterns/style_t2';
@import './skin';
```

## Build

We use **Webpack4** to compile styles. JS files must be included separately using the Drupal way.
1. Run `npm install` inside **docroot** folder
2. Run `webpack --name $siteName --env $tier --watch` 

Where: 
- *$siteName* - it's name of created site 
- *$tier* - required tier type (only t1 or t2 allowed)

After compilation, **main.bundle.css** will be added to automatically created **css** folder inside _project directory_.
