#!/usr/bin/env bash

yarn -g i 

PORT=3000
BS_PORT=4000
for DIR in $(find docroot/sites -iname 'investis-drupal-t*'); do
    cd /var/www/html/${DIR}/themes;
    yarn --silent --pure-lockfile install;
    ./node_modules/.bin/gulp --env=${NODE_ENV} publish;
    ./node_modules/.bin/gulp --env=${NODE_ENV} 2>>/dev/stderr 1>>/dev/stdout &
    ((++PORT))
    ((++BS_PORT))
done

tail -f /dev/stdout /dev/stderr
