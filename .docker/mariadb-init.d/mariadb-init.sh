#!/usr/bin/env bash

echo "
  CREATE DATABASE IF NOT EXISTS ${MYSQL_DATABASE_1};
  CREATE DATABASE IF NOT EXISTS ${MYSQL_DATABASE_2};
  GRANT ALL ON ${MYSQL_DATABASE_1}.* TO ${MYSQL_USER}@'%';
  GRANT ALL ON ${MYSQL_DATABASE_2}.* TO ${MYSQL_USER}@'%';
" | "${mysql[@]}"

for f in /mariadb-dumps/*; do
  case "$f" in
    *t1*)
      echo "$0: importing dump $f to t1";
      time "${mysql[@]}" ${MYSQL_DATABASE_1} < "$f";
    ;;
    *t2*)
      echo "$0: importing dump $f to t2";
     time "${mysql[@]}" ${MYSQL_DATABASE_2} < "$f";
    ;;
  esac
done

# Clean up cache tables.
"${mysql[@]}" -e "SELECT concat('TRUNCATE TABLE \`', TABLE_NAME, '\`;') FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'cache%'"