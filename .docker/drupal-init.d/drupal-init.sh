#!/usr/bin/env bash

set -ex

DRUPAL_ROOT="${APP_ROOT}/${DOCROOT_SUBDIR}"

gotpl "/docker-entrypoint-init.d/sites.php.tmpl" > "${DRUPAL_ROOT}/sites/sites.php"
mkdir -p "${APP_ROOT}/drush/sites"
gotpl "/docker-entrypoint-init.d/drush.site.yml.tmpl" > "${APP_ROOT}/drush/sites/docker.site.yml"

composer global require hirak/prestissimo
composer build

export DRUPAL_HASH_SALT=$(drush eval "var_dump(Drupal\Component\Utility\Crypt::randomBytesBase64(55))")
if [[ -d "${DRUPAL_ROOT}/sites/investis-drupal-t1" ]]; then
    SYNC_DIR="sync-base/investis-drupal-t1" DB_NAME=${DB_NAME_T1} gotpl "/docker-entrypoint-init.d/settings.php.tmpl" > "${DRUPAL_ROOT}/sites/investis-drupal-t1/settings.php"
fi
if [[ -d "${DRUPAL_ROOT}/sites/investis-drupal-t2" ]]; then
    SYNC_DIR="sync-base/investis-drupal-t2" DB_NAME=${DB_NAME_T2} gotpl "/docker-entrypoint-init.d/settings.php.tmpl" > "${DRUPAL_ROOT}/sites/investis-drupal-t2/settings.php"
fi
