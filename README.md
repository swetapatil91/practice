# Investis Drupal

## Get Started

### Docker

Project is using docker compose based on wodby stack.

For easily start place DB dumps to `.docker/mariadb-dumps` directory,
this dumps should contains `t1` and `t2` in their names for automatically 
import, example:
```bash
$ ls -l .docker/mariadb-dumps/
investis_drupal_t1_qa_Thu.sql
investis_drupal_t2_qa_Thu.sql
```

_* mariadb require some time to import databases, so just relax and take tea_

To run the site use `make` command inside `.docker` directory, example:

```bash
$ make up
```

To stop the container run `make down`

For drush use `make drush`, by default it will run command for t1 environment, 
you can also use `make drush.t1` and `make drush.t2` commands.
Also, you can use `make drupal` or `make drupal.t2` for running drupal console.

_* if you want to pass options to drush use quotes: `make drush "st --help"`._

**Voilà!**

Your sites are accessible by: http://t1.investis.localhost/ 
and http://t2.investis.localhost/

##### Config override

For changing base url or using non-standard port create file `.env` file with 
next content:

```dotenv
PROJECT_BASE_URL=investis.localhost
PROJECT_PORT=8888
SITE_DEFAULT='@docker.t1'
NODE_ENV=stage
```

Sub-domans `t1.` and `t2.` will be added automatically.

##### XDEBUG

To add xdebug or other development-related settings,
create `docker-compose.override.yml` with next content:

```yaml
version: "3"

services:
  php:
    environment:
      PHP_XDEBUG: 1
      PHP_XDEBUG_DEFAULT_ENABLE: 1
      PHP_XDEBUG_REMOTE_AUTOSTART: 1
      PHP_XDEBUG_REMOTE_CONNECT_BACK: 1
      PHP_XDEBUG_IDEKEY: PHPSTORM
      PHP_IDE_CONFIG: serverName=investis
#      PHP_XDEBUG_REMOTE_HOST: host.docker.internal # Docker 18.03+ Mac/Win
      PHP_XDEBUG_REMOTE_HOST: 172.17.0.1 # Linux
#      PHP_XDEBUG_REMOTE_HOST: 10.254.254.254 # macOS, Docker < 18.03
#      PHP_XDEBUG_REMOTE_HOST: 10.0.75.1 # Windows, Docker < 18.03
      PHP_XDEBUG_REMOTE_LOG: /tmp/php-xdebug.log
```

##### FAQ

###### Slow command run

If `make shell` command runs very slow, add `127.0.0.1	localunixsocket.local`
to `/etc/hosts` file, it will help docker-compose resolve host quickly.

###### Firefox can't open anything on `*.investis.localhost`

Firefox has own dns resolving system and doesn't supports localhost subdomains,
to fix it open `about:config` and find next setting `network.dns.localDomains`
and set value: 
```
t1.investis.localhost, t2.investis.localhost, front.investis.localhost, mailhog.investis.localhost, portainer.investis.localhost
```